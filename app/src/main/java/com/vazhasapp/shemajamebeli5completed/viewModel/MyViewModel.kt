package com.vazhasapp.shemajamebeli5completed.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.vazhasapp.shemajamebeli5completed.model.JsonSubListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import java.io.InputStream

class MyViewModel(application: Application) : AndroidViewModel(application) {

    var json: String? = null

    private val context = getApplication<Application>()
    private val inputStream: InputStream = context.assets.open("register.json")

    private var _fieldsLiveData = MutableLiveData<MutableList<MutableList<JsonSubListItem>>>()
    val fieldsLiveData: LiveData<MutableList<MutableList<JsonSubListItem>>> = _fieldsLiveData

    fun init() {
        viewModelScope.launch(Dispatchers.Default) {
            parseJson()
        }
    }

    private fun parseJson(): MutableList<MutableList<JsonSubListItem>> {
        var myJson = mutableListOf<MutableList<JsonSubListItem>>()
        try {
            json = inputStream.bufferedReader().use { it.readText() }

            val jsonFirstArray = JSONArray(json)
            for (i in 0 until jsonFirstArray.length()) {
                var jsonSecondArray = jsonFirstArray.getJSONArray(i)
                myJson.add(mutableListOf())
                for (j in 0 until jsonSecondArray.length()) {
                    val jsonObject = jsonSecondArray.getJSONObject(j)

                    myJson[i].add(
                        JsonSubListItem(
                            jsonObject.getInt("field_id"),
                            jsonObject.getString("field_type"),
                            jsonObject.getString("hint"),
                            jsonObject.getString("icon"),
                            jsonObject.getBoolean("is_active"),
                            jsonObject.getBoolean("required"),
                            if (jsonObject.has("keyboard"))
                                jsonObject.getString("keyboard")
                            else null
                        )
                    )
                }
                _fieldsLiveData.postValue(myJson)
            }
            return myJson
        } catch (e: JSONException) {
            e.printStackTrace()
            return myJson
        }
    }
}