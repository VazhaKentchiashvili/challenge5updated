package com.vazhasapp.shemajamebeli5completed.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli5completed.R
import com.vazhasapp.shemajamebeli5completed.adapter.MainRecyclerAdapter
import com.vazhasapp.shemajamebeli5completed.databinding.FragmentMainBinding
import com.vazhasapp.shemajamebeli5completed.model.JsonSubListItem
import com.vazhasapp.shemajamebeli5completed.viewModel.MyViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val myViewModel: MyViewModel by viewModels()
    private val myAdapter = MainRecyclerAdapter()

    //private val user = mutableMapOf<Int, String>()
    private var myJson = mutableMapOf<Int, JsonSubListItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        myViewModel.init()
    }

    private fun init() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = myAdapter
            setHasFixedSize(true)
        }
        listeners()
        binding.btnRegister.setOnClickListener {
            checkEnteredInfo()
        }
    }

    private fun checkEnteredInfo() {
        binding.recyclerView.children.forEach {
            it.findViewById<RecyclerView>(R.id.childRecycelrView).children.forEach { nested ->
                //val fieldId = nested.findViewById<EditText>(R.id.etRegister)?.text.isNullOrEmpty()
                 val editTexts = nested.findViewById<EditText>(nested.id)

                myJson.forEach {

                    if (it.key == nested.id && it.value.required && editTexts.text.isEmpty()) {
                        Toast.makeText(requireContext(), "Saved", Toast.LENGTH_SHORT).show()
                        return
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Please fill required fields",
                            Toast.LENGTH_SHORT
                        ).show()
                        return
                    }
                }
            }
        }
    }

    private fun listeners() {
        myViewModel.fieldsLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it)

            it.forEach {
                it.forEach { jsonObj ->
                    myJson[jsonObj.fieldId] = jsonObj
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}