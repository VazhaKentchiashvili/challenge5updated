package com.vazhasapp.shemajamebeli5completed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli5completed.databinding.MainRecyclerCardViewBinding
import com.vazhasapp.shemajamebeli5completed.model.JsonSubListItem

class MainRecyclerAdapter : RecyclerView.Adapter<MainRecyclerAdapter.MainRecyclerViewHolder>() {

    private val mainRecyclerList = mutableListOf<List<JsonSubListItem>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MainRecyclerViewHolder(
            MainRecyclerCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {

        holder.bind()
        setChildRecyclerInMain(holder.childRecycler, mainRecyclerList[position])
    }

    override fun getItemCount() = mainRecyclerList.size

    inner class MainRecyclerViewHolder(private val binding: MainRecyclerCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
            lateinit var childRecycler: RecyclerView
            fun bind() {
                childRecycler = binding.childRecycelrView
            }
        }

    private fun setChildRecyclerInMain(
        childRecycler: RecyclerView,
        childRecyclerList: List<JsonSubListItem>
    ) {
        val childAdapter = ChildRecyclerAdapter(childRecyclerList)
        childRecycler.adapter = childAdapter
        childRecycler.layoutManager =
            LinearLayoutManager(childRecycler.context)
    }

    fun setData(mainRecyclerList: List<List<JsonSubListItem>>) {
        this.mainRecyclerList.clear()
        this.mainRecyclerList.addAll(mainRecyclerList)
        notifyDataSetChanged()
    }
}