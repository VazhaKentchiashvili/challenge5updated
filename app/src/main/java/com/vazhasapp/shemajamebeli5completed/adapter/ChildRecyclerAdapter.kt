package com.vazhasapp.shemajamebeli5completed.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli5completed.R
import com.vazhasapp.shemajamebeli5completed.databinding.ChildChooserCardBinding
import com.vazhasapp.shemajamebeli5completed.databinding.ChildItemCardViewBinding
import com.vazhasapp.shemajamebeli5completed.model.JsonSubListItem


class ChildRecyclerAdapter(var childRecyclerList: List<JsonSubListItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val genderSpinner = listOf("Male", "Female")
    private val dateSpinner = listOf(1997,1998,1999,2000,2001,2002,2003,2004)

    companion object {
        private const val CHOOSER_VIEW = 0
        private const val EDIT_TEXT_VIEW = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            ChildChooserRecyclerViewHolder(
                ChildChooserCardBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        } else {
            ChildRecyclerViewHolder(
                ChildItemCardViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChildRecyclerViewHolder -> holder.bind()
            is ChildChooserRecyclerViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = childRecyclerList.size

    override fun getItemViewType(position: Int): Int {
        return when (childRecyclerList[position].fieldType) {
            "chooser" -> CHOOSER_VIEW
            "input" -> EDIT_TEXT_VIEW
            else -> -1
        }
    }

    inner class ChildRecyclerViewHolder(private val binding: ChildItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentChildField: JsonSubListItem
        fun bind() {
            currentChildField = childRecyclerList[adapterPosition]

            binding.root.hint = currentChildField.hint
            binding.root.id = currentChildField.fieldId

            binding.etRegister.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {

                }
            })
        }
    }

    inner class ChildChooserRecyclerViewHolder(private val binding: ChildChooserCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentChildField: JsonSubListItem

        fun bind() {
            currentChildField = childRecyclerList[adapterPosition]
            binding.root.id = currentChildField.fieldId

            val mAdapterBirthDate = ArrayAdapter(binding.chooserView.context, android.R.layout.simple_spinner_dropdown_item, dateSpinner)
            binding.chooserView.adapter = mAdapterBirthDate
        }

    }
}