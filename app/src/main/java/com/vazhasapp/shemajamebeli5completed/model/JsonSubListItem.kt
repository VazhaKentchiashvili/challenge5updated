package com.vazhasapp.shemajamebeli5completed.model

import com.google.gson.annotations.SerializedName

data class JsonSubListItem(
    @SerializedName("field_id")
    val fieldId: Int,
    @SerializedName("field_type")
    val fieldType: String,
    val hint: String,
    val icon: String,
    @SerializedName("is_active")
    val isActive: Boolean,
    val required: Boolean,
    val keyboard: String? = null,
)